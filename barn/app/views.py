from django.shortcuts import render
from .models import *
from .forms import AnimalForm

# Create your views here.
def barn_view(request):
	ark = Animal.objects.all()
	if request.method == 'POST':
		if 'delete' in request.POST:
			Animal.objects.get(id =request.POST['delete']).delete()
			
		form = AnimalForm(request.POST)
		if form.is_valid():
			animal = form.save()
	form = AnimalForm()

	return render(request, 'barn.html', {
			'ark' : ark,
			'form' : form
		})