from django.db import models

# Create your models here.
class Animal(models.Model):
	GENDER_MALE = 'M'
	GENDER_FEMALE = 'F'
	GENDER_OTHER = 'O'
	GENDER_CHOICES = (
		(GENDER_MALE, 'Mann'),
		(GENDER_FEMALE, 'Kvinne'),
		(GENDER_OTHER, 'Annet')
	)
	gender = models.CharField(
		max_length=1,
		choices=GENDER_CHOICES,
		default=GENDER_OTHER
	)
	SPECIES_COW = 0
	SPECIES_CHCIKEN = 1
	SPECIES_PIG = 2
	SPECIES_SHEEP = 3
	SPECIES_HORSE = 4
	SPECIES_RABBIT = 5
	SPECIES_DONKEY = 6
	SPECIES_POLARBEAR = 7
	SPECIES_WOLF = 8
	SPECIES_PARROT = 9
	SPECIES_OCELOT = 10
	SPECIES_LLAMA = 11
	SPECIES_MULE = 12
	SPECIES_BAT = 13
	SPECIES_SQUID = 14
	SPECIES_CHOICES = (
		(SPECIES_COW, 'Ku'),
		(SPECIES_CHCIKEN, 'Kylling'),
		(SPECIES_PIG, 'Gris'),
		(SPECIES_SHEEP, 'Sau'),
		(SPECIES_HORSE, 'Hest'),
		(SPECIES_RABBIT, 'Kanin'),
		(SPECIES_DONKEY, 'Esel'),
		(SPECIES_POLARBEAR, 'Isbjørn'),
		(SPECIES_WOLF, 'Ulv'),
		(SPECIES_PARROT, 'Papegøye'),
		(SPECIES_OCELOT, 'Ozelot'),
		(SPECIES_LLAMA, 'Llama'),
		(SPECIES_MULE, 'Muldyr'),
		(SPECIES_BAT, 'Flaggermus'),
		(SPECIES_SQUID, 'Blekksprut')
	)
	species = models.IntegerField(choices=SPECIES_CHOICES)
	name = models.CharField(max_length=255)
	age = models.PositiveIntegerField()
	description = models.TextField(null=True, blank=True)
	# TODO delete everything