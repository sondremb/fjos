from django import forms
from .models import Animal

class AnimalForm(forms.ModelForm):
	class Meta:
		model = Animal
		fields = ['name', 'species', 'age', 'gender', 'description']
		labels = {
			'name' : 'Navn',
			'species' : 'Art',
			'age' : 'Alder',
			'gender' : 'Kjønn',
			'description' : 'Beskrivelse'
		}